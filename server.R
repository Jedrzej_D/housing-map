library(shiny)
library(leaflet)
library(leaflet.extras)
library(maps)
library(dplyr)
library(ggplot2)
library(jsonlite)
library(data.table)
library(RCurl)
library(shinyBS)
library(h2o)

# get function to calculate zone
#source('./isinzone.R')
## load model for prediction
#load('./lmmodel')
## create reactive object to be reused for subfunctions

## import dataset
  
##check and get only data witout NAs at coordinations

## Set secondary keys for faster access
df = fread('./final_dataset.csv',header = T, stringsAsFactors = TRUE)
colnames(df)[1:2] = c('id','price')
colnames(df)=tolower(colnames(df))
df = df[complete.cases(df[,c('price','lat','long')]),]
df = df[sample(nrow(df)),]
setkeyv(df,c('id','long','lat','price','nroom'))
zoom = reactiveValues()
zoom$df=df



server=shinyServer(function(input, output, session){
  
  session$onSessionEnded(function() {
    try({h2o.shutdown(prompt = FALSE)}, silent=TRUE)
  })
  

  custMarker = makeIcon(
    iconUrl = 'dot.png',
    iconWidth = 20,
    iconHeight = 20
  )

 
  ## BASE MAP 
  output$livemap <- renderLeaflet({
    
    ## load geojson of milan areas
    zoom$spdf <- paste0(readLines('spdf.json'), collapse = "")
    ## set zoom levels for each area
    zoom$zoom <- c(14,13,13,13,13,13,13,13,13)
  
    ## set approx. center of areas and id
    state_lat = c(45.466170,45.5013, 45.4815, 45.4491, 45.4233, 45.4423, 45.47, 45.4967, 45.5096)
    state_long = c(9.186518,9.2231, 9.2369, 9.2399, 9.1940, 9.1498, 9.1047, 9.1378, 9.1831 )
    state_id = 1:9
    zoom$mapInd = data.frame(state_lat,state_long,state_id)
    zoom$quart <- paste0(readLines('quartieri.json'), collapse = "")
    
    ## main basic leaflet map
    ## important to have preferCanvas options otherwise objects rendered as DOMs
    leaflet(options = leafletOptions(worldCopyJump=TRUE, preferCanvas = TRUE)) %>%
      addBootstrapDependency() %>% 
            ## restrict not to move to far outside
            setMaxBounds(lng1 = 9.4, lng2 = 9, lat1 = 44.5, lat2 = 46.5) %>% 
            
            ## type of map - tiles
            ## On default uses HEREmaps
            ## 
       
            addProviderTiles('HERE.terrainDay',
                             options = providerTileOptions(minZoom = 11, maxZoom = 20,
                                                           updateWhenZooming = TRUE,
                                                           updateWhenIdle = TRUE,
                                                           app_id = 'EbmTri4ueyJb7zbExC0A',
                                                           app_code = 'jp4nHbHA--n7JcPObk1X-w')
                             ) %>%
            ## Initial View
            setView(lng = 9.2, lat = 45.45, zoom = 12) %>% 
            addScaleBar() %>%
            addResetMapButton() %>% 
            addFullscreenControl() %>% 
            addSearchOSM(options = searchOptions(
              hideMarkerOnCollapse = TRUE, autoCollapse = TRUE,
              collapsed = TRUE
      )) 
  
       
    
  })
  
  ## Fallback to OSM if Here fails
  ## Need recheck in case HERE maps address would change
  observe({
    out = tryCatch(is.character(getURL("http://1.aerial.maps.cit.api.here.com")), error=function(e) 'Blocked here server')
    out2 = tryCatch(is.character(getURL("http://2.aerial.maps.cit.api.here.com")), error=function(e) 'Blocked here server')
    out3 = tryCatch(is.character(getURL("http://3.aerial.maps.cit.api.here.com")), error=function(e) 'Blocked here server')
    
    if(out=='Blocked here server' | out2=='Blocked here server' | out3=='Blocked here server')
      leafletProxy('livemap') %>% clearTiles() %>% 
      addTiles(options = providerTileOptions(minZoom = 11, maxZoom = 17,
                                            updateWhenZooming = TRUE,
                                            updateWhenIdle = TRUE  ))
  })
  
  ## DATA FOR MARKERS
  ## Dynamic filtered data based on location and user filters
  data = reactiveValues()
  
  ## list of input to listen on
  listen2 = reactive(
    list(input$livemap_bounds, 
         input$price_input, 
         input$marker_input, 
         input$room_input, 
         input$meter_input)
  )
  
  observeEvent(listen2(),{
    req(input$price_input)
    req(input$livemap_bounds)
    #output$msg2=renderText({paste0('zzz', (input$livemap_bounds$north))})
    
    ## Bounds of currently viewed map by the user
    ## Lowers number of objects to render by user's browser
    high_lat = input$livemap_bounds$north
    low_lat = input$livemap_bounds$south
    left = input$livemap_bounds$east
    right = input$livemap_bounds$west
    
    ## Used standard R subsetting 
    ## For some reason dynamic dplyr not working properly 
    data$df = head(zoom$df[zoom$df$lat<high_lat & 
                             zoom$df$lat>low_lat &
                             zoom$df$long<left &
                             zoom$df$long>right &
                             # check for interval filters
                             zoom$df$price>input$price_input[1] &
                             zoom$df$price<input$price_input[2] &
                             zoom$df$sqmeter>input$meter_input[1] &
                             zoom$df$sqmeter<input$meter_input[2] &
                             zoom$df$nroom>input$room_input[1] &
                             zoom$df$nroom<input$room_input[2],], input$marker_input)
  })
  
  mark_listen = reactive({
    list(input$pleth,data$df,input$heatmap)
  })
  

   

  ## WHEN DATA UPDATED CHANGE MARKERS
  observeEvent(mark_listen(),{

    #output$msg3 = renderText({paste0('Markers')})
    req(input$marker_input)
    req(data$df)
    if(input$pleth==FALSE & input$heatmap==FALSE){

          leafletProxy("livemap") %>% 
                      clearGroup('General') %>%
            
                      ## On custom leaflet.js will create custom html path icon instead of a circle
                      ## Define layerId for PopUps
                      ## Popus from functions are laggy
                      addCircleMarkers(lat=data$df$lat, 
                                       lng=data$df$long,
                                       opacity = 1,
                                       color = 'black', 
                                       fillColor = ifelse(data$df$price>3500,'green','yellow'),
                                       radius = 0.8, 
                                       weight = 1, 
                                       fillOpacity = 0.7, 
                                       layerId = data$df$id,
                                       group = 'General'
      
                       ) 
    }
    zoom$h2o = 1
  })
  

  ### HEATMAP 
  ## Unknown problem with color/price adding where sum gives incorrect color
  ## Thats why radius shrinked to a point
  observeEvent(input$heatmap,{
    if(input$heatmap==TRUE){
      
      ## create same gradient as the one used by addHeatmap
      colorGradient <- colorRampPalette(c("blue", "green", "yellow", "red"))(10)
      pal <- colorNumeric(
        palette = colorGradient,
        domain = zoom$df$price
      )
      
      leafletProxy('livemap') %>%
        clearHeatmap() %>%  
        clearMarkers() %>% 
        clearGroup('Reverse') %>% 
        addHeatmap(lat=zoom$df$lat, 
                   lng=zoom$df$long,
                   intensity = zoom$df$price, 
                   blur=2,
                   radius = 8, 
                   max=max(zoom$df$price,na.rm = T),
                   layerId = 'legend') %>%
        addLegend("bottomright", pal = pal, values = zoom$df$price
        )

    ## clear if heatmap disabled
    }else{
      leafletProxy('livemap') %>% clearHeatmap() %>% clearControls()
    }
  })
  
 
  

  json_list = reactive({
    list(input$livemap_zoom,input$pleth)
  })
  
  # Map with mean price per subdistrict
  observeEvent(input$pleth,{
      if(input$pleth==TRUE){
          if(!exists('zoom$sp')){
            zoom$sp = fromJSON('quartieri.json')
          }
          # color gradient
          colorGradient <- colorRampPalette(c('yellow','red', 'black'))(10)
          pal <- colorNumeric(
            palette = colorGradient,
            domain = zoom$sp$features$properties$mean
          )
          
          leafletProxy('livemap') %>% 
            clearGeoJSON() %>% 
            clearMarkers() %>% 
            clearControls() %>% 
            clearGroup('Reverse') %>% 
            addGeoJSONChoropleth(
              zoom$quart,
              valueProperty = 'mean',
              scale = c('yellow','red', 'black'), steps = 20,
              labelProperty='NIL', mode = 'e',
              weight=1, fillOpacity = 0.7,
              highlightOptions = highlightOptions(
                      weight=2, color='#000000',
                      fillOpacity=1, opacity =1,
                      bringToFront=TRUE, sendToBack=TRUE),
              
              layerId = 'pleth'
            ) %>% 
            addLegend("bottomright", pal = pal, values = zoom$sp$features$properties$mean,
                      title='Mean price'
            )
          
        }else
        {}
  }
  )
  
  # GEOJSON OF MILAN ZONES
  observeEvent(json_list(),{
    req(input$livemap_zoom)
    if(input$pleth==TRUE){}
    else{
    # output$table = renderDataTable(data$df)

    zoom$prev_zoom = input$livemap_zoom

    #output$msg3 = renderText({paste0('Zoom:',input$livemap_zoom)})
    ## remove geojson as hard to click markers with it
    if(input$livemap_zoom>14){
      leafletProxy("livemap") %>% 
        clearGeoJSON() %>% 
        clearControls() %>% 
        clearGroup('pleth') 
    }else{

      # add geojson of Milan areas
      leafletProxy("livemap") %>%
                  clearGeoJSON()   %>% 
                  clearControls() %>% clearGroup('pleth') %>% 
                  clearTopoJSON() %>% clearShapes() %>% 
                  leaflet.extras::addGeoJSONv2(zoom$spdf,
                                     weight = 1.2,
                                     smoothFactor = 0.5,
                                     opacity = 1.0,
                                     color = '#fff',
                                     fillOpacity = 0.12,
                                     fill = TRUE,
                                     fillColor = 'sandybrown',
                                     highlightOptions = highlightOptions(color='#888',
                                                                         fillColor='#eee',
                                                                         fillOpacity = 0.3,
                                                                         opacity = 0.3,
                                                                         bringToFront = TRUE)
        )
    }
    }

  })
  
  ## clear markers added by users clicks 
  observeEvent(input$resetMarker,{
    leafletProxy("livemap") %>% clearGroup('Reverse')
  })
  
  
  observeEvent(input$livemap_click,{

    zoom$p2 = input$livemap_marker_click
    ## assign arbitrary value so if on longitute does not return length 0 error
    if(length(zoom$p2$lng)==0){
      zoom$p2$lng = 180
    }
    #output$msg4=renderText({paste0(str(zoom$p2))})
    ## Add points only if geojson zones not present
    if(input$livemap_zoom<=14){
      NULL
    }else{
     cur_obs = 0
     p = zoom$p2
     cur_obs = data$df[data$df$id==p$id,]
     #output$msg4=renderText({paste0((cur_obs$price))})
     #output$msg4=renderText({paste0((p$id))})
     ## if true then clicked place is a marker otherwise only add marker and display coords
     if(zoom$p2$lng == input$livemap_click$lng){
         leafletProxy('livemap') %>%
           clearPopups() %>%
           addPopups(p$lng,
                     p$lat,
                     HTML(paste("<table class='table table-striped'><tbody>
                                    <tr><td>Price/month</td><td>",paste0(cur_obs$price,"€"),"</td></tr>
                                    <tr><td>Rooms</td><td>",cur_obs$nroom,"</td></tr>
                                    <tr><td>Square Meters</td><td>",cur_obs$sqmeter,"</td></tr>
                                    <tr><td>Condominium</td><td>",paste0(cur_obs$condom,'€'),"</td></tr>
                                    <tr><td>Heating</td><td>",cur_obs$heating,"</td></tr>
                                    <tr><td>Lat.</td><td>",p$lat,"</td></tr>
                                    <tr><td>Long.</td><td>",p$lng,"</td></tr>
                                    </tbody></table>")),
                     options = popupOptions(closeOnClick = NULL, maxHeight = 500, maxWidth = 500)
                     
           )
       }else{
       p = input$livemap_click
       leafletProxy('livemap') %>%
         addMarkers(lng = p$lng, lat=p$lat, icon = custMarker, group='Reverse') %>%
         addPopups(p$lng,
                   p$lat,
                   HTML(paste("<table class='table table-striped'><tbody>
                               <tr><td>Lat.</td><td>",round(p$lat,4),"</td></tr>
                               <tr><td>Long.</td><td>",round(p$lng,4),"</td></tr>
                               </tbody></table>")),
                   options = popupOptions(closeOnClick = FALSE, maxHeight = 80, maxWidth = 80),
                   group = 'Reverse'
         )
     }
 
  }
 
    
    })
  
  ## ZOOM ON GEOJSON CLICK
  observeEvent(input$livemap_geojson_click,{
    if(input$pleth==FALSE){
          req(input$livemap_zoom)
          if(input$livemap_zoom>14){
             
          }else{
            click <- input$livemap_geojson_click
             #output$msg2 <- renderText({paste0(input$livemap_geojson_click)})
            
            if(is.null(click))
              return()
            idx <- which(zoom$mapInd$state_id == click$properties$ZONADEC)
            # output$msg4 = renderText({paste0('IDX:', idx)})
            ## Get zoom level for the state
            z <- zoom$zoom[idx]
            ## Get state name to render new map
            
            leafletProxy("livemap") %>%
             
              flyTo(lng = zoom$mapInd$state_long[idx],
                    lat = zoom$mapInd$state_lat[idx],
                    zoom = z, 
                    options = list(duration=0.6))
            
          }
    }else{
      
      click <- input$livemap_geojson_click
      if(is.null(click))
        return()
      #output$msg2 <- renderText({paste0(input$livemap_geojson_click)})
      leafletProxy('livemap') %>%
        clearPopups() %>%

        addPopups(click$lng,
                  click$lat,

                  HTML(paste("<table class='table table-striped'><thead><tr><td>price</td><td>Name</td></tr></thead><tbody><tr><td>", click$properties$mean, "</td><td>", click$properties$NIL, "</td></tr></tbody></table>"))

        )
    }
  })


  observeEvent(input$livemap_click,{
    output$recentlat = renderText({paste0('Recently clicked: ',round(as.numeric(input$livemap_click[1]),6))})
    output$recentlong = renderText({paste0('Recently clicked: ',round(as.numeric(input$livemap_click[2]),6))})
  })
  
  pred_listen = reactive({
    list(input$prooms, input$psqmeter, input$plat, input$plong, input$pbuildage, 
         input$pcondom,input$pfloor,input$pphoto)
  })
  ## Price Prediction

    observeEvent(pred_listen(),{
      #output$msg4=renderText({paste0((is.na(check56) | is.na(check55) | is.na(check57)))})
      check = tryCatch(as.numeric(input$prooms),warning=function(w) w)
      check2 = tryCatch(as.numeric(input$psqmeter),warning=function(w) w)
      check3 = tryCatch(as.numeric(input$plat),warning=function(w) w)
      check4 = tryCatch(as.numeric(input$plong),warning=function(w) w)
      check55 = tryCatch(as.numeric(input$pcondom),warning=function(w) w)
      check56 = tryCatch(as.numeric(input$pfloor),warning=function(w) w)
      check57 = tryCatch(as.numeric(input$pphoto),warning=function(w) w)
      if(!is.list(check) & !is.na(check[[1]]) & 
         !is.list(check2) & !is.na(check2[[1]]) &
         !is.list(check3) & !is.na(check3[[1]]) & 
         !is.list(check4) & !is.na(check4[[1]]) &
         (is.na(check56) | is.na(check55) | is.na(check57))
         ){
        
          #output$textoneout = renderText({paste0(as.character(points_in_zona(input$livemap_click[2],input$livemap_click[1])))})
          data.to.pred = (data.frame(as.numeric(input$prooms),as.numeric(input$psqmeter), as.numeric(input$plat), as.numeric(input$plong)))
          colnames(data.to.pred) = c('nroom','sqmeter','lat','long')
          output$prediction = renderText({paste0('Predicted price: ', round(as.vector(h2o.predict(zoom$lowDim,as.h2o(data.to.pred))),0),"€/month")})
              }
 
       if(length(zoom$ModelRun)<1)
       if((!is.list(check) & !is.na(check[[1]])) |
          (!is.list(check2) & !is.na(check2[[1]])) |
          (!is.list(check3) & !is.na(check3[[1]])) | 
          (!is.list(check4) & !is.na(check4[[1]]) )
       ){
         h2orun$resume()
         
       }
       
  })
  
    h2orun = observe(suspended = TRUE,{
      isolate({h2o.init(nthreads = 1, max_mem_size = '100M')})
      zoom$lowDim = h2o.loadModel('./FinalFinalLowDim')
      zoom$FullDim = h2o.loadModel('./FinalFinalModel')
      zoom$ModelRun = 11
      NULL
    })
    
    
  
    pred_listen_full = reactive({
      list(input$pcondom, input$pfloor, input$pphoto,
           input$ptpprop,
           input$pstatus,
           input$pheat,
           input$pac,
           input$penclass,
           input$pcontr,
           input$pfurnished,
           input$pkitchen,
           input$prooms, input$psqmeter, input$plat, input$plong, input$pbuildage)
    })
    
    
    observeEvent(pred_listen_full(),{
      check = tryCatch(as.numeric(input$prooms),warning=function(w) w)
      check2 = tryCatch(as.numeric(input$psqmeter),warning=function(w) w)
      check3 = tryCatch(as.numeric(input$plat),warning=function(w) w)
      check4 = tryCatch(as.numeric(input$plong),warning=function(w) w)
      check55 = tryCatch(as.numeric(input$pcondom),warning=function(w) w)
      check56 = tryCatch(as.numeric(input$pfloor),warning=function(w) w)
      check57 = tryCatch(as.numeric(input$pphoto),warning=function(w) w)
      if(!is.list(check) & !is.na(check[[1]]) & 
         !is.list(check2) & !is.na(check2[[1]]) &
         !is.list(check3) & !is.na(check3[[1]]) &
         !is.list(check4) & !is.na(check4[[1]]) &
         !is.list(check55) & !is.na(check55[[1]]) &
         !is.list(check56) & !is.na(check56[[1]]) &
         !is.list(check57) & !is.na(check57[[1]])
      ){


        #output$textoneout = renderText({paste0(as.character(points_in_zona(input$livemap_click[2],input$livemap_click[1])))})
        data.to.pred = data.frame(as.numeric(input$prooms),
                                   as.numeric(input$psqmeter), 
                                   as.numeric(input$plat), 
                                   as.numeric(input$plong), 
                                   as.numeric(input$pcondom),
                                   as.numeric(input$pfloor),
                                   as.numeric(input$pphoto),
                                   as.factor(input$ptpprop),
                                   input$pstatus,
                                   input$pheat,
                                   input$pac,
                                   input$penclass,
                                   input$pcontr,
                                   input$pfurnished,
                                   input$pkitchen
                                   )
        colnames(data.to.pred) = c('nroom','sqmeter','lat','long',
                                   'condom','floor','photosnum','tpprop','status',
                                   'heating','ac','enclass','contr',
                                   'furnished', 'kitchen')
        output$prediction = renderText({paste0('Predicted price: ', round(as.vector(h2o.predict(zoom$FullDim,as.h2o(data.to.pred))),0),"€/month")})
      }

        
    })
    
    
    
})



